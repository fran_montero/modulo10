separadorEjercicios("- 1. hasId:");
//Ejercicio 1: Implementa una función llamada hasId que admita como parámetro un objeto y compruebe si dicho objeto tiene una propiedad llamada id (debe devolver booleano true/false).
const ej1A = {
    id: 122,
    name: "prueba 1",
};
const ej1B = {
    idUser: 122,
    name: "prueba 2",
};
const hasId = ({id}) => (id != null ? true : false);
console.log("El objeto 'ej1A' tiene atributo id: " + hasId(ej1A));
console.log("El objeto 'ej1B' tiene atributo id: " + hasId(ej1B));

separadorEjercicios("- 2. head:");
// Ejercicio 2: Implementa una función llamada head tal que, dado un array como entrada, devuelva el primer elemento.
const ej2 = ["uno", "dos", "tres", "cuatro"];
const head = ([primero]) => primero;
console.log(head(ej2));


separadorEjercicios("- 3. tail:");
// Ejercicio 3: Implementa una función llamada tail tal que, dado un array como entrada, devuelva un nuevo array con todos los elementos menos el primero.
const ej3 = ["uno", "dos", "tres", "cuatro"];
const tail = ([, ...rest]) => rest;
const ej3Result = tail(ej3);
console.log(ej3Result);

separadorEjercicios("- 4. swapFirstToLast:");
// Ejercicio 4: Implementa una función llamada swapFirstToLast tal que, dado un array como entrada, devuelva un nuevo array donde el primer elemento ha sido colocado en la última posición.
const ej4 = ["uno", "dos", "tres", "cuatro"];
const swapFirstToLast =  ([primero, ...rest]) => [rest + "," + primero];
const ej4Result = swapFirstToLast(ej4);
console.log(ej4Result);

separadorEjercicios("- 5. excludeId:");
// Ejercicio 5: Implementa una función llamada excludeId tal que, dado un objeto como entrada, devuelva dicho objeto clonado excepto la propiedad id si la hubiera.
const ej5A = {
    id: 1,
    name: "prueba 1",
    email: "correo 1"
};
const ej5B = {
    name: "prueba 2",
    email: "correo 2"
};
const excludeId = ({id,...rest}) => rest;
const ej5ResultA = excludeId(ej5A);
const ej5ResultB = excludeId(ej5B);
console.log("El objeto 'ej5A' tiene atributo id: ");
console.log(ej5ResultA);
console.log("El objeto 'ej5A' no tiene atributo id: ");
console.log(ej5ResultB);

separadorEjercicios("- 6. wordsStartingWithA:");
// Ejercicio 6: Implementa una función llamada wordsStartingWithA tal que, dado un array de palabras como entrada, devuelva otro array filtrado con aquellas palabras que empiecen por a.
const ej6 = ["amor", "odio", "aire", "tierra"];
const wordsStartingWithA = palabras => palabras.filter(palabra => palabra[0] == "a");
console.log(wordsStartingWithA(ej6));

separadorEjercicios("- 7. concat:");
// Ejercicio 7: Implementa una función llamada concat tal que admita múltiples argumentos de tipo string y devuelva otro string con la concatenación de todos, separados por |.
const concat = (...strings) => {
    let concatenado = "";
    for (const string of strings) {
        if (concatenado != "") {
            concatenado += "|";
        } 
        concatenado += string;
    }
    return concatenado;
};
console.log(concat("amor", "odio", "aire", "tierra"));

separadorEjercicios("- 8. multArray:");
//Ejercicio 8: Implementa una función llamada multArray que admita un array de números (arr) y otro parámetro que sea un número (x) y devuelva un nuevo array donde cada elemento ha sido multiplicado por x.
const ej8 = [2,3,5];
function multArray (array, num) {
    return array.map((num => x => x * num)(num));
};
console.log(multArray(ej8, 5))

separadorEjercicios("- 9. calcMult:");
//Ejercicio 9: Implementa una función llamada calcMult que admita múltiples números como argumento y devuelva como resultado el producto de todos ellos.
const calcMult = (...numeros) => {
    let multiplicacion = 1;
    for (const numero of numeros) {
        multiplicacion *= numero;
    }
    return multiplicacion;
};
console.log("3*5*7*9 = " + calcMult(3,5,7,9));






function separadorEjercicios(titulo){
    console.log("");
    console.log("------------------------------------------------------------");
    console.log(titulo);
    console.log("------------------------------------------------------------");
    
}